package com.itau.kafka.consumerempresa.services;

import org.springframework.stereotype.Service;

@Service
public class EmpresaService {
    public Boolean validaCapital(Double capital){
        if (capital > 1000000){
            return true;
        }
        return false;
    }
}
