package com.itau.kafka.consumerempresa.consumers;

import com.itau.kafka.consumerempresa.clients.EmpresaClient;
import com.itau.kafka.cadastraempresa.models.Empresa;
import com.itau.kafka.consumerempresa.clients.EmpresaDTO;
import com.itau.kafka.consumerempresa.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmpresaConsumer {
    @Autowired
    EmpresaClient empresaClient;

    @Autowired
    EmpresaService empresaService;

    @Autowired
    private KafkaTemplate<String, String> producer;

    @KafkaListener(topics = "spec2-angela-valentim-2", groupId = "angela-kafka")
    public void receber(@Payload Empresa empresa) {
        EmpresaDTO empresaDTO = new EmpresaDTO();
        try {
            empresaDTO = empresaClient.getByCnpj(empresa.getCnpj());
            if (empresaDTO.getStatus()!="ERROR") {
                empresa.setCapital(empresaDTO.getCapital_social());
                empresa.setStatusCadastro(empresaService.validaCapital(empresa.getCapital()));
                String resultado = String.format("%.2f", empresa.getCapital());
                String log="Empresa "+empresa.getCnpj()+ " nome "+empresaDTO.getNome()+" de capital "+resultado
                        +" status "+empresa.getStatusCadastro();
                enviarAoKafka(log);
            }
            else{
                System.out.println("Empresa "+empresa.getCnpj()+" não encontrada na Receita Federal - Erro: "+empresaDTO.getMessage());
            }
        }
        catch (Exception e){
            System.out.println("Consulta da empresa "+empresa.getCnpj()+" apresentou problemas - Erro: "+empresaDTO.getMessage());
        }
    }

    public void enviarAoKafka(String log) {
        producer.send("spec2-angela-valentim-3", log);
    }

}
