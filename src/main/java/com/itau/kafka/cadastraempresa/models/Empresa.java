package com.itau.kafka.cadastraempresa.models;

public class Empresa {
    private String cnpj;
    private String nome;
    private Double capital;
    private Boolean statusCadastro;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getCapital() {
        return capital;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public Boolean getStatusCadastro() {
        return statusCadastro;
    }

    public void setStatusCadastro(Boolean statusCadastro) {
        this.statusCadastro = statusCadastro;
    }
}
